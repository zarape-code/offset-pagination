import express, { Request, Response } from "express"
import { Connection } from "typeorm"
import { UserController } from "../controller/UserController"
import { getDBConnection } from "../db"

export const userRouter = express.Router()

let connection: Connection;
let userAPI: UserController;
getDBConnection().then(conn => {
  connection = conn
  userAPI = new UserController(connection);
})

userRouter.get('/', async (req: Request, res: Response) => {
  const offset = parseInt(String(req.query.offset))
  const limit = parseInt(String(req.query.limit))
  res.send(await userAPI.getUsers(offset, limit))
})
