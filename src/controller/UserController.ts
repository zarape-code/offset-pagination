import { Connection } from "typeorm";
import { User } from "../entity/User";
import { IUser } from "../model/IUser";

export class UserController {

  private connection: Connection;
  private DEFAULT_OFFSET = 0;
  private DEFAULT_LIMIT = 10;

  constructor(connection: Connection) {
    this.connection = connection
  }

  async getUsers(offset: number, limit: number): Promise<Array<IUser>> {
    offset = isNaN(offset) ? this.DEFAULT_OFFSET : offset
    limit = isNaN(limit) ? this.DEFAULT_LIMIT : limit
    return await this.connection.manager.find(User, {
      skip: offset,
      take: limit
    });
  }

}