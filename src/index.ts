import express, { Request, Response } from "express"
const app = express()
const port = 3000
import { userRouter } from "./router";

app.get('/', (_req: Request, res: Response) => {
  res.send('Paginación por desplazamiento [Offset pagination] demo 😎')
})

app.use('/user', userRouter)

app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`)
})