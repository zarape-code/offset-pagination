export interface IUser {
  id?: number;
  name?: string;
  lastName?: string;
  email: string;
  gender: string;
  ipAddress: string;
  avatar: string;
}