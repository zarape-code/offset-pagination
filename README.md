# Offset pagination

API con paginación por desplazamiento [más info...](http://blog.zarape.io/)

## Iniciar el proyecto
Arrancar en modo desarrollo con hot reload
```
$ npm start
```
Compilar proyecto para `prod` usando el compilador de TypeScript
```
$ npm run build
```

## Estructura del proyecto
```sh
\                   # archivos de configuración
|-src               # agrupador del código fuente
    |-controller    # lógica del proyecto
    |-db            # conexión a BD
    |-entity        # mapeo a tablas de BD
    |-model         # entidades TypeScript
    |-router        # express router
    |-index.ts      # archivo de arranque
```
## Lógica de paginación
Aunque hay toda una estructura para exponer esto como `web service` que lee la información de la BD; la paginación se ejecuta en el archivo `UserController.ts`
```ts
async getUsers(offset: number, limit: number): Promise<Array<IUser>> {
  offset = isNaN(offset) ? this.DEFAULT_OFFSET : offset
  limit = isNaN(limit) ? this.DEFAULT_LIMIT : limit
  return await this.connection.manager.find(User, {
    skip: offset, // definición de desplazamiento
    take: limit   // definición de cantidad de elementos
  });
}
```

## Mock data
Los 1,000 registros almacenados en `database.sqlite` fueron obtenidos de [Mockaroo](https://www.mockaroo.com/)

## Tecnología empleada
* [Express](https://expressjs.com/) - Web Framework
* [TypeScript](https://www.typescriptlang.org/) - Tipos para JS
* [SQLite](https://www.sqlite.org/) - Base de datos
* [TypeORM](https://typeorm.io/) - ORM

## Extensiones de VSCode usadas
* [vscode-sqlite](https://github.com/AlexCovizzi/vscode-sqlite) - SQLite explorer
* [REST Client](https://github.com/Huachao/vscode-restclient) - Alternativa a Postman para crear llamadas a la API
___
### Autor
Creado por [Ethien Salinas](https://www.linkedin.com/in/ethiensalinas/) para zarape.io 🇲🇽